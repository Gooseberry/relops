



public class RelOps {
    public static boolean and(boolean relExpr1,boolean relExpr2){
        return relExpr1 && relExpr2;
    }
    public static boolean equalTo(double x,double y){
        return x == y;
    }
    public static boolean greaterThan(double x, double y){
        return x > y;
    }
    public static boolean greaterThanOrEqualTo(double x, double y){
        return x >= y;
    }
    public static boolean lesserThan(double x, double y){
        return x < y;
    }
    public static boolean lesserThanOrEqualTo(double x, double y){
        return x <= y;
    }
    public static boolean not(boolean relExp1){
        return !relExp1;
    }
    public static boolean notEqualTo(double x, double y){
        return x != y;
    }
    public static boolean or(boolean relExp1, boolean relExp2){
        return relExp1 || relExp2;
    }
}