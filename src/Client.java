import java.util.Scanner;

public class Client {
    private static Scanner kb = new Scanner(System.in);

    public static void main(String[] args) {
        openMenu();
    }

    public static void openMenu() {
        Scanner kb = new Scanner(System.in);
        int choice;

        do {
            System.out.println("\n What would you like to do? " +
                    "\n 1. Is Equal Function" +
                    "\n 2. Is Greater Function"+
                    "\n 3. Is Equal or Greater Function" +
                    "\n 4. Is Lesser Function" +
                    "\n 5. Is Lesser or Equal Function" +
                    "\n 6. Will Negate" +
                    "\n 7. Is not Equal Function" +
                    "\n");


            choice = kb.nextInt();
            switch (choice) {
                case 1:

                    isEqual();
                    break;
                case 2:
                    isGreater();
                    break;
                case 3:
                    isEqualOrGreater();
                    break;
                case 4:
                    isLesser();
                    break;
                case 5:
                    isLesserOrEqual();
                    break;
                case 6:
                    negateExpr();
                    break;
                case 7:
                    isNotEqual();
                    break;
                case 8:
                    atLeastOneExprIsTrue();
                    break;
                case 9:
                    isFirstNumberGreaterThanOrEqualToTheSecond();
                    break;
                case 10:
                    isFirstNumberLesserThanOrEqualToTheSecond();
                    break;
                case 11:
                    exOr();
                    break;
                case 12:
                    isFirstNumberLesserThanSecondNumber();
                    break;
                case 13:
                    isFirstNumberGreaterThanSecondNumber();
                    break;
                case 14:
                    outOfExclusiveRange();
                    break;
                case 15:
                    outOfInclusiveRange();
                    break;
                case 16:
                    withinExclusiveRange();
                    break;
                case 17:
                    withinInclusiveRange();
                    break;
                case 18:
                    System.out.println("Thank you for using this application!");
                    System.exit(0);
                    break;


            }

        } while (choice !=100 );

    }

    public static void isEqual() {

        System.out.println("Enter the first value");
        int value = kb.nextInt();
        System.out.println("Enter the next value");
        int value2 = kb.nextInt();

    }

    public static void isGreater() {

        System.out.println("Enter the first value");
        int value = kb.nextInt();
        System.out.println("Enter the next value");
        int value2 = kb.nextInt();


    }

    public static void isEqualOrGreater() {

        System.out.println("Enter the first value");
        int value = kb.nextInt();
        System.out.println("Enter the next value");
        int value2 = kb.nextInt();


    }

    public static void isLesser() {

        System.out.println("Enter the first value");
        int value = kb.nextInt();
        System.out.println("Enter the next value");
        int value2 = kb.nextInt();

    }

    public static void isLesserOrEqual() {

        System.out.println("Enter the first value");
        int value = kb.nextInt();
        System.out.println("Enter the next value");
        int value2 = kb.nextInt();

    }

    public static void negateExpr() {

        System.out.println("Enter the first value");
        int value = kb.nextInt();
        System.out.println("Enter the next value");
        int value2 = kb.nextInt();
    }
    public static void isNotEqual() {

        System.out.println("Enter the first value");
        int value = kb.nextInt();
        System.out.println("Enter the next value");
        int value2 = kb.nextInt();
    }
    public static void atLeastOneExprIsTrue() {

        System.out.println("Enter the first value");
        int value = kb.nextInt();
        System.out.println("Enter the next value");
        int value2 = kb.nextInt();
    }
    public static void isFirstNumberGreaterThanOrEqualToTheSecond() {

        System.out.println("Enter the first value");
        int value = kb.nextInt();
        System.out.println("Enter the next value");
        int value2 = kb.nextInt();
    }
    public static void isFirstNumberLesserThanOrEqualToTheSecond() {

        System.out.println("Enter the first value");
        int value = kb.nextInt();
        System.out.println("Enter the next value");
        int value2 = kb.nextInt();
    }
    public static void exOr() {

        System.out.println("Enter the first value");
        int value = kb.nextInt();
        System.out.println("Enter the next value");
        int value2 = kb.nextInt();
    }
    public static void isFirstNumberLesserThanSecondNumber() {

        System.out.println("Enter the first value");
        int value = kb.nextInt();
        System.out.println("Enter the next value");
        int value2 = kb.nextInt();
    }
    public static void isFirstNumberGreaterThanSecondNumber() {

        System.out.println("Enter the first value");
        int value = kb.nextInt();
        System.out.println("Enter the next value");
        int value2 = kb.nextInt();
    }
    public static void outOfExclusiveRange() {

        System.out.println("Enter the first value");
        int value = kb.nextInt();
        System.out.println("Enter the next value");
        int value2 = kb.nextInt();
    }
    public static void outOfInclusiveRange() {

        System.out.println("Enter the first value");
        int value = kb.nextInt();
        System.out.println("Enter the next value");
        int value2 = kb.nextInt();
    }
    public static void withinExclusiveRange() {

        System.out.println("Enter the first value");
        int value = kb.nextInt();
        System.out.println("Enter the next value");
        int value2 = kb.nextInt();
    }
    public static void withinInclusiveRange() {

        System.out.println("Enter the first value");
        int value = kb.nextInt();
        System.out.println("Enter the next value");
        int value2 = kb.nextInt();
    }

}