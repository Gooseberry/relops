public class FacadeClass {
    public static boolean isBothTrue(boolean x,boolean y){
        return RelOps.and(x,y);
    }


    public static boolean isEqual(double x,double y) {
        return RelOps.equalTo(x,y);
    }

    public static boolean isGreater(double x,double y) {
        return RelOps.greaterThan(x,y);
    }

    public static boolean isEqualOrGreater(double x,double y) {
        return RelOps.greaterThanOrEqualTo(x,y);
    }

    public static boolean isLesser(double x,double y) {
        return RelOps.lesserThan(x,y);
    }

    public static boolean isLesserOrEqual(double x,double y) {
        return RelOps.lesserThanOrEqualTo(x,y);
    }

    public static boolean negateExpr(boolean expr1) {
        return RelOps.not(expr1);
    }

    public static boolean isNotEqual(double x,double y) {
        return RelOps.notEqualTo(x,y);
    }

    public static boolean atLeastOneExprIsTrue(double x,double y) {
        return RelOps.lesserThanOrEqualTo(x,y);
    }
    public static boolean isFirstNumberGreaterThanOrEqualToTheSecond(double x, double y){
        return RelQuantifiers.atLeast(x,y);
    }

    public static boolean isFirstNumberLesserThanOrEqualToTheSecond(double x, double y){
        return RelQuantifiers.atMost(x,y);
    }

    public static boolean exOr(boolean x, boolean y){
        return RelQuantifiers.exclusiveOr(x,y);
    }

    public static boolean isFirstNumberLesserThanSecondNumber(double x, double y){
        return RelQuantifiers.notLessThan(x,y);
    }

    public static boolean isFirstNumberGreaterThanSecondNumber(double x, double y){
        return RelQuantifiers.notMoreThan(x,y);
    }

    public static boolean outOfExclusiveRange(double x, double min, double y){
        return RelQuantifiers.outOfRangeExclusive(x,min,y);
    }

    public static boolean outOfInclusiveRange(double x, double min, double y){
        return RelQuantifiers.outOfRangeInclusive(x,min,y);
    }

    public static boolean withinExclusiveRange(double x, double min, double y){
        return RelQuantifiers.withInExclusive(x,min,y);
    }

    public static boolean withinInclusiveRange(double x, double min, double y){
        return RelQuantifiers.withInInclusive(x,min,y);
    }

}

