

public class RelQuantifiers {
    public static boolean atLeast(double x, double y){
        return RelOps.greaterThanOrEqualTo(x,y);
    }
    public static boolean atMost(double x, double y){
        return RelOps.lesserThanOrEqualTo(x,y);
    }
    public static boolean exclusiveOr(boolean relExpr1, boolean relExpr2){
        return  RelOps.and(RelOps.or(RelOps.not(relExpr1),RelOps.not(relExpr2)),RelOps.or(relExpr1,relExpr2));
    }
    public static boolean notLessThan(double x, double y){
        return RelOps.greaterThan(x,y);
    }
    public static boolean notMoreThan(double x, double y){
        return RelOps.lesserThan(x,y);
    }
    public static boolean outOfRangeExclusive(double x, double min, double max){
       return RelOps.not(RelOps.and(RelOps.greaterThan(x,min+1), RelOps.lesserThan(x, max -1 )));

    }
    public static boolean outOfRangeInclusive(double x, double min, double max){
        return RelOps.and(RelOps.lesserThanOrEqualTo(x,min),RelOps.greaterThanOrEqualTo(x,max));
    }
    public static boolean withInExclusive(double x, double min, double max){
        return RelOps.and(RelOps.greaterThanOrEqualTo(x,min+1), RelOps.lesserThanOrEqualTo(x,max-1));
    }
    public static boolean withInInclusive(double x, double min, double max){
        return RelOps.and(RelOps.greaterThan(x,min), RelOps.lesserThan(x,max));
    }
}